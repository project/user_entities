# User Entities (user_entities)
by Michael Bagnall - mrbagnall@icloud.com

A Drupal module for associating user entities with every other kind of entity. Use case is to save references to nodes, taxonomy or any other entity to users.
